# PyBackligth

PyBacklight is a console backligth controller. It provides notifications showing the percentage of backligth
It's composed by a daemon that runs and a CLI to interact with it

- pybacklight (daemon)
- pybacklightc (CLI)

## Requirements:

make a backligth.rules in /etc/udev/rules.d/

```bash
ACTION=="add", SUBSYSTEM=="backlight", KERNEL=="intel_backlight", RUN+="/bin/chgrp video /sys/class/backlight/%k/brightness"
ACTION=="add", SUBSYSTEM=="backlight", KERNEL=="intel_backlight", RUN+="/bin/chmod g+w /sys/class/backlight/%k/brightness"

```

Add your user to the video group

```bash
usermod -a -G video $USER 
```

## Installl

```bash
sudo python3 setup.py install
```

Append pybacklight to your init script and use pybacklightc to control it

```bash
pybacklightc minus
pybacklightc plus
```
