class excessBackLigth(Exception):
    pass

class minusZeroBackLigth(Exception):
    pass

class Backlight:

    def __init__ (self):
        self.maxBackLight = None
        self.currentBackLight = None
        self.rate = None

    def decreaseBackLigth(self, filename):
        try:
            self.currentBackLight = self.currentBackLight - self.rate
            if self.currentBackLight < 0:
                raise minusZeroBackLigth
        except minusZeroBackLigth:
            self.currentBackLight = 0
        self.writeFile(filename)

    def increaseBackLight(self, filename):
        try:
            self.currentBackLight += self.rate
            if self.maxBackLight < self.currentBackLight:
                raise excessBackLigth
        except excessBackLigth:
            self.currentBackLight = self.maxBackLight
        self.writeFile(filename)

    def writeFile(self, filename):
        with open(filename, "w") as file:
            file.write(str(self.currentBackLight))

    def setMaxBackligth(self, filename):
        with open(filename) as file:
            self.maxBackLight = int(file.read())

        self.rate = int(self.maxBackLight * 0.05)

    def setCurrentBackLigth(self, filename):
        with open(filename) as file:
            self.currentBackLight = int(file.read())

    def getPercentage(self):
        percentage = self.currentBackLight / self.maxBackLight
        percentage = percentage * 100
        return int(percentage)
