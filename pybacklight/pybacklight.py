import sys
import socket

from pybacklight.backligthControl import backlight
from pybacklight.notification import notification


HOST = ''
PORT = 50007
MAXFILE = "/sys/class/backlight/intel_backlight/max_brightness"
CURRENTFILE = "/sys/class/backlight/intel_backlight/brightness"

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((HOST, PORT))
s.listen(1)
backlight = backlight.Backlight()
notification = notification.Notification()
backlight.setCurrentBackLigth(CURRENTFILE)
backlight.setMaxBackligth(MAXFILE)

def main():
    while 1:
        conn, addr = s.accept()
        data = conn.recv(1024)
        print(data)

        if not data: pass

        if data == b'minus':
            backlight.decreaseBackLigth(CURRENTFILE)
        elif data == b'plus':
            backlight.increaseBackLight(CURRENTFILE)
        else:
            pass
        message = backlight.getPercentage()
        notification.setNotification(message)
        notification.show()
