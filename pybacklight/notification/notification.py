import gi
gi.require_version("Notify", "0.7")

from gi.repository import Notify

class Notification:
    def __init__(self):
        Notify.init("pyxbackligth")

    def setNotification(self, message):
        outputMessage = self.prepareMessage(message)
        self.Notification = Notify.Notification.new("Backligth", outputMessage)
        self.Notification.set_app_name("pyxbacklight")
        self.Notification.props.id = 12

    def show(self):
        self.Notification.show()

    def prepareMessage(self, message):
        progress = ''
        iterations = int(message) // 5
        for i in range(0, iterations -1):
            progress += '='
        progress += '|'
        for i in range(iterations, 20):
            progress += '-'
        return """{} {}%""".format(progress, message)
