#! /bin/python3 
import sys
import socket


HOST = ''
PORT = 50007

if __name__ == "__main__":
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((HOST, PORT))
    try:
        message = sys.argv[1]
    except:
        message = ''
    s.send(message.encode())
    s.close()
