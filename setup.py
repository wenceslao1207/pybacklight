from setuptools import setup, find_packages
from setuptools.command.install import install
from shutil import copyfile
import os


class MyInstall(install):
    def run(self):
        install.run(self)
        src = './pybacklight/pybacklightc.py'
        dst = '/usr/local/bin/pybacklightc'
        copyfile(src, dst)
        os.system("chmod +x "+dst)


setup(
    name="pybacklight",
    version="0.1.0",
    description="Simple backligth controller",
    author="Wenceslao1207",
    author_email="wenceslao1207@protonmail.com",
    packages=find_packages(),
    entry_points={
        'console_scripts': [
            'pybacklight=pybacklight.pybacklight:main',
        ]
    },
    cmdclass={'install': MyInstall},
)
